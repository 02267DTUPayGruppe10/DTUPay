import com.google.gson.JsonObject;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.json.JSONObject;

import java.util.Scanner;
//This program prints out CVR and address of a company in CVR register of DK/Norway. Input is name of company and country in which it is registered.
public class Task {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Type in name of company: ");
        String inputSearch = scanner.next();
        System.out.println("Type in country (dk / no)");
        String inputCountry = scanner.next();

        HttpResponse<JsonNode> response = Unirest.post("https://cvrapi.dk/api?")
                .queryString("search", inputSearch)
                .queryString("country", inputCountry)
                .header("accept", "application/json")
                .asJson();

        JSONObject json = response.getBody().getObject();


        System.out.println("CVR: " + json.get("vat") + "\n Addresse: " + json.get("address") );
    }
}
