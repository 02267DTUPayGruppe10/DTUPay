import java.io.File;
import org.openImaj

public class main {
    public static void main(String[] args) {
        MBFImage image = ImageUtilities.readMBF(new File("resources/RobotUdenRetning.jpg"));
        MBFImage imageHSV = Transforms.RGB_TO_HSV(image);
        DisplayUtilities.display(imageHSV);
    }
}
