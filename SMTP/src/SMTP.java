import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.Scanner;

public class SMTP {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        BufferedReader reader = null;
        BufferedWriter writer = null;


        try{
            String host = "127.0.0.1"; //Localhost
            System.out.println("Connecting");

            while(true){
                Socket socketClient = new Socket(host, 25); //Localhost on port 25
                Scanner scanner1 = new Scanner(socketClient.getInputStream());
                //Reader and writer that reads and writes to/from server
                reader = new BufferedReader(new InputStreamReader(socketClient.getInputStream()));
                writer = new BufferedWriter(new OutputStreamWriter(socketClient.getOutputStream()));

                String serverMsg;
                //Checks if server has written anything
                if((serverMsg = reader.readLine()) !=null){
                    System.out.println("Server : " + serverMsg);
                    writer.write("HELO JUSTUS\r\n");
                    writer.flush();

                    serverMsg = reader.readLine();
                    System.out.println("Server: " + serverMsg);
                    System.out.println("Server: enter senders email");

                    String emailFrom = scanner.next();
                    writer.write("MAIL FROM: " + emailFrom + "\r\n");
                    writer.flush();

                    serverMsg = reader.readLine();
                    System.out.println("Server: " + serverMsg);
                    System.out.println("Server: enter recievers email address" );

                    String emailTo = scanner.next();
                    writer.write("RCPT TO: " + emailTo +  "\r\n");
                    writer.flush();

                    serverMsg = reader.readLine();
                    System.out.println("Server: " + serverMsg);

                    writer.write("DATA\r\n");
                    writer.flush();

                    serverMsg = reader.readLine();
                    System.out.println("Client: " + serverMsg);
                    System.out.println("Server: Enter content: ");
                    String subjectString = scanner.next();
                    writer.write("SUBJECT: " + subjectString + "\r\n");
                    writer.flush();

                    String data;
                    boolean endMessage = false;

                    do {
                        data = scanner.next();
                        writer.write(data + "\r\n");

                        if (data.charAt(0) == '.') {
                            endMessage = true;
                        }
                    } while (!endMessage);
                    writer.flush();

                    serverMsg = reader.readLine();
                    System.out.println("Server: " + serverMsg);
                }
            }
        } catch(Exception e){
            System.out.println("Client: Failed connecting to server");
            System.out.println(e.getMessage());
        }
    }

}
