import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class SSLSMTP {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        BufferedReader reader = null;
        BufferedWriter writer = null;


        try{
            SSLSocketFactory factory = (SSLSocketFactory) SSLSocketFactory.getDefault();

            SSLSocket socket = (SSLSocket) factory.createSocket("smtp.gmail.com", 465);

            PrintWriter out = new PrintWriter(
                    new BufferedWriter(
                            new OutputStreamWriter(
                                    socket.getOutputStream())));

            out.println("GET / HTTP/1.0");
            out.println();
            out.flush();

            /*
             * Make sure there were no surprises
             */
            if (out.checkError())
                System.out.println(
                        "SSLSocketClient:  java.io.PrintWriter error");

            /* read response */
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                            socket.getInputStream()));

            String inputLine;
            while ((inputLine = in.readLine()) != null)
                System.out.println(inputLine);

            in.close();
            out.close();
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
/*
        try{
            String host = "127.0.0.1"; //Localhost
            System.out.println("Connecting");

            while(true){
                Socket socketClient = new Socket(host, 25); //Localhost on port 25
                Scanner scanner1 = new Scanner(socketClient.getInputStream());
                //Reader and writer that reads and writes to/from server
                reader = new BufferedReader(new InputStreamReader(socketClient.getInputStream()));
                writer = new BufferedWriter(new OutputStreamWriter(socketClient.getOutputStream()));

                String serverMsg;
                //Checks if server has written anything
                if((serverMsg = reader.readLine()) !=null){
                    System.out.println("Server : " + serverMsg);
                    writer.write("HELO JUSTUS\r\n");
                    writer.flush();

                    serverMsg = reader.readLine();
                    System.out.println("Server: " + serverMsg);
                    System.out.println("Server: enter senders email");

                    String emailFrom = scanner.next();
                    writer.write("MAIL FROM: " + emailFrom + "\r\n");
                    writer.flush();

                    serverMsg = reader.readLine();
                    System.out.println("Server: " + serverMsg);
                    System.out.println("Server: enter recievers email address" );

                    String emailTo = scanner.next();
                    writer.write("RCPT TO: " + emailTo +  "\r\n");
                    writer.flush();

                    serverMsg = reader.readLine();
                    System.out.println("Server: " + serverMsg);

                    writer.write("DATA\r\n");
                    writer.flush();

                    serverMsg = reader.readLine();
                    System.out.println("Client: " + serverMsg);
                    System.out.println("Server: Enter content: ");
                    String subjectString = scanner.next();
                    writer.write("SUBJECT: " + subjectString + "\r\n");
                    writer.flush();

                    String data;
                    boolean endMessage = false;

                    do {
                        data = scanner.next();
                        writer.write(data + "\r\n");

                        if (data.charAt(0) == '.') {
                            endMessage = true;
                        }
                    } while (!endMessage);
                    writer.flush();

                    serverMsg = reader.readLine();
                    System.out.println("Server: " + serverMsg);
                }
            }
        } catch(Exception e){
            System.out.println("Client: Failed connecting to server");
            System.out.println(e.getMessage());
        }*/
    }
}
