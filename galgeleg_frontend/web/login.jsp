<%--
  Created by IntelliJ IDEA.
  User: marina
  Date: 03-03-2020
  Time: 09:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>GAGLELEG</title>
    <link href="login.css" rel="stylesheet" type="text/css">
  </head>
  <body alink="#7fff00">
  <h1 class="header">
    GALGELEG
  </h1>
  <p class="introduction">
    Før du kan gå i gang med spillet skal du logge ind.
  </p>
  <div class="login_container">
  <form action="action_page.php" method="post">
    <div class="email-field">
      <label><b>Brugernavn</b></label>
      <input type="email" placeholder="Skriv din email" required >
    </div>

    <div class="password-field">
      <label><b>Password</b></label>
      <input type="password" placeholder="Skriv dit kodeord" required>
    </div>

    <p></p>

    <div class="container">
      <span class="glemt">Glemt <a href="glemtLogin.jsp" style="color: black">kodeord?</a></span>
    </div>

    <p></p>
    <form>
      <button type="button" class="button"><script src="startside.jsp"></script>Log in</button>
    </form>

    
    <div>
      <button type="button" class="button"><a href="startside.jsp"></a>Log in</button>
    </div>

    <p></p>
  </form>
  </div>
  </body>
</html>
